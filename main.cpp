#include <iostream>
#include <thread>
#include <chrono>
#include "helpers.h"
#include "window.h"
#include "texture.h"
#include "tile.h"
#include "character.h"
#include "world.h"
using namespace std;
		//Character myPacman(0,0,Pacman);
int main()
{
    // SpriteSheet Filename
       string spriteFilename = SPRITEFILENAME; // Leave this line

    // Setup and Load Texture object here
	Texture myTexture;
	myTexture.loadFile(spriteFilename, 20, 20);
    bool quit = false;
	//Tile pm(0,0,{{1,1},{1,2},{1,1},{1,3}},Pacman,1,1);
	int frame=0;
	 World myWorld(MAZEFILENAME, myTexture.tileWidth(),myTexture.tileHeight());
	//vector<Tile> testTiles = getTestTiles();
	
	while(!quit){
        // Handle any SDL Events
	SDL_Event e;
        // Such as resize, clicking the close button,
        //  and process and key press events.
	while(SDL_PollEvent(&e)){
	if (e.type==SDL_QUIT){
	quit= true;
	}
	myWorld.pacman.handle_event(e);
	}
	myWorld.UpdateWorld();
	SDL_SetRenderDrawColor(myTexture.myWin.sdlRenderer, 0, 0, 0, 255);
	SDL_RenderClear(myTexture.myWin.sdlRenderer);
		//pm.render(&myTexture, frame);
	//for (int k=0; k < testTiles.size();k++){
		//testTiles[k].render(&myTexture,frame);
		
		//}
	myWorld.render(&myTexture, frame);
	SDL_RenderPresent(myTexture.myWin.sdlRenderer);
	this_thread::sleep_for(chrono::milliseconds(75));
	frame++;

        // Update the Game State Information

        // Draw the current state to the screen.
    }

    return 0;
}
