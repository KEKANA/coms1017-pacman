#include "world.h"
#include "helpers.h"
#include "character.h"

#include <string>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <iostream>

using namespace std;
/**
 * Constructs the World object loading a maze from the supplied file.
 * It should initialise the \ref{map} array with the relevant tiles as well
 * as the pacman and ghost objects.
 *
 * @param filename - File from which the maze should be read.
 * @param tileWidth - Width of each tile
 * @param tileHeight - Height of each tile
 */
World::World(string filename, int tileWidth, int tileHeight)
    : food(0), points(0), ready(true), pacman(0,0,Pacman)
{
    ifstream f(filename);
    if(!f.is_open())
        throw runtime_error("Couldn't open maze file " + filename);
    // Code to read in the file...
    string myStr;
    char myChar;

    int counter = -1;
    int x, y;

    f>>rows;
    f>>cols;

    vector<Tile>temp;
      while(!f.eof())
      {
	getline(f, myStr);
       
        for(size_t i=0; i<myStr.length(); i++)
	 {
           x=i*tileWidth;
           y=counter*tileHeight;

           myChar=myStr[i];

              if(myChar=='x')
		temp.push_back(makeTile(x, y, Wall, Up));
		else if(myChar=='.')
		{
		 temp.push_back(makeTile(x, y, Food, Up));
		  food++;
		}
		else if(myChar==' ')
		 temp.push_back(makeTile(x, y, Blank, Up));
		else if(myChar=='0')
                {
		 temp.push_back(makeTile(x, y, Blank, Up));
		 Character DemiPac(x, y, Pacman);
		 pacman=DemiPac;
		}
		else if(myChar=='1')
		 temp.push_back(makeTile(x, y, Blank, Up));
		else if(myChar=='2')
		 temp.push_back(makeTile(x, y, Blank, Up));
                else if(myChar=='3')
		 temp.push_back(makeTile(x, y, Blank, Up));
 		else if(myChar=='4')
		 temp.push_back(makeTile(x, y, Blank, Up)); 							
         }
	  counter++;
	  if(temp.size()==static_cast<size_t>(cols))
	  {
            maze.push_back(temp);
	    temp.clear();
          }	
    }
}

/**
 * Renders the World to the ::sdlRenderer buffer.
 * It calls the respective render functions on each tile first.
 * Following this, it calls the pacman and ghost objects to render
 * them above the background.
 * @param frame [optional] An optional frame number to pass to the objects to handle animation.
 */
void World::render(Texture *t, int frame)
{
  for(int i=0; i<rows; i++)
  {
    for(int j=0; j<cols; j++)
    {
      maze[i][j].render(t, frame);
    }
  }
  pacman.render(t, frame);	
 
}

/**
 * This function is responsible for advancing the game state.
 * Pacman and the ghosts should be moved (if possible). If pacman is
 * captured by a ghost pacman.dead should be set to true. If pacman eats
 * a food pellet the relevant totals should be updated.
 *
 * @return The same value as World::ready, indicating whether the game is finished.
 */
bool World::UpdateWorld()
{
  SDL_Rect newRect = pacman.getNextPosition();
  SDL_Rect wallTile;
  
  bool isOverlapping = false;

 for(int i=0; i<rows; i++)
  {
    for(int j=0; j<cols; j++)
    {
        wallTile.x = maze[i][j].x;
        wallTile.y = maze[i][j].y;
	wallTile.h = maze[i][j].tileHeight;
	wallTile.w = maze[i][j].tileWidth;
	
	if(maze[i][j].myType == Wall) 
	   if( collision (newRect, wallTile, 3, 2))
	   {
	       isOverlapping = true;
	       break;
	   }
     }
  }

  if( isOverlapping == false )
      {
          pacman.x=newRect.x;
          pacman.y=newRect.y;
	  SDL_Rect foodTile;

	  for(int i=0; i<rows; i++)
          {
             for(int j=0; j<cols; j++)
             {
        	foodTile.x = maze[i][j].x;
		foodTile.y = maze[i][j].y;
		foodTile.h = maze[i][j].tileHeight;
		foodTile.w = maze[i][j].tileWidth;

		if( maze[i][j].myType == Food)
		if ( collision (newRect, foodTile, 5, 5))
		{
		    maze[i][j] = makeTile(foodTile.x, foodTile.y, Blank, Down);
		    points++;
		    cout << "Score - " << points << endl;
		}
	      }
	   }
       }
       
       if ( food == points )
	    return true;
	
	return false;

}
